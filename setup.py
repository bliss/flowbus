#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name='tango-bronkhorst-flowbus',
    version='1.23',
    description="Bronkhorst Flowbus Tango server",
    #packages=["flowbusDS"],
    package_dir={"":"src" },
    py_modules=["flowbusDS"],
    #entry_points={"console_scripts": [
    #    "FlowbusDS = flowbusDS.flowbusDS:main",
    #]},
    entry_points={"console_scripts": [
        "FlowbusDS = flowbusDS:main",
    ]},
)
